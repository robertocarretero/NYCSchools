//
//  ThemeColors.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import SwiftUI

struct ThemeColors {
    static let cloud = Color("cloud", bundle: Bundle.main)
    static let snow = Color("snow", bundle: Bundle.main)
    static let darksteel = Color("darksteel", bundle: Bundle.main)
    static let gunmetal = Color("gunmetal", bundle: Bundle.main)
    static let iron = Color("iron", bundle: Bundle.main)
    static let tangerine = Color("tangerine", bundle: Bundle.main)
    static let mimosa = Color("mimosa", bundle: Bundle.main)
    static let steel = Color("steel", bundle: Bundle.main)
    static let tin = Color("tin", bundle: Bundle.main)
}
