//
//  SchoolDetailsView.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import SwiftUI

struct SchoolDetailsView: View {
    
    let schoolDetailsModel: SchoolDetailsModel
    let schoolFormatHelper = SchoolFormatHelper()
    let testResultsFormatHelper = TestResultsFormatHelper()
    
    var body: some View {
        VStack {
            ScrollView {
                titleAddressSection()
                
                HStack {
                    Spacer()
                        .frame(height: 1.0)
                        .background(ThemeColors.iron)
                }
                .padding(.horizontal, 16.0)
                
                overviewParagraphSection()
                
                HStack {
                    Spacer()
                        .frame(height: 1.0)
                        .background(ThemeColors.iron)
                }
                .padding(.horizontal, 16.0)
                
                testResultsSection()
            }
        }
    }
    
    func testResultsSection() -> some View {
        VStack {
            VStack {
                HStack {
                    Text("SAT Test Results:")
                        .font(.caption.bold())
                        .foregroundColor(ThemeColors.cloud)
                        .fixedSize(horizontal: false, vertical: true)
                    Spacer()
                }
                .padding(.leading, 8.0)
                .padding(.trailing, 8.0)
                .padding(.top, 8.0)
                .padding(.bottom, 2.0)
                
                HStack {
                    Spacer()
                        .frame(height: 1.0)
                        .background(ThemeColors.steel)
                }
                .padding(.horizontal, 8.0)
                
                VStack {
                    
                    HStack {
                        Text("Number Of Takers:")
                            .font(.caption.bold())
                            .foregroundColor(ThemeColors.tangerine)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    HStack {
                        Text(testResultsFormatHelper.takers(testResults: schoolDetailsModel.testResult))
                            .font(.caption2)
                            .foregroundColor(ThemeColors.mimosa)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    
                }
                .padding(.leading, 8.0)
                .padding(.trailing, 8.0)
                
                HStack {
                    Spacer()
                        .frame(height: 1.0)
                        .background(ThemeColors.gunmetal)
                }
                .padding(.horizontal, 8.0)
                
                VStack {
                    
                    HStack {
                        Text("Average Reading Score:")
                            .font(.caption.bold())
                            .foregroundColor(ThemeColors.tangerine)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    HStack {
                        Text(testResultsFormatHelper.reading(testResults: schoolDetailsModel.testResult))
                            .font(.caption2)
                            .foregroundColor(ThemeColors.mimosa)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    
                }
                .padding(.leading, 8.0)
                .padding(.trailing, 8.0)
                .padding(.bottom, 4.0)
                
                HStack {
                    Spacer()
                        .frame(height: 1.0)
                        .background(ThemeColors.gunmetal)
                }
                .padding(.horizontal, 8.0)
                
                VStack {
                    
                    HStack {
                        Text("Average Writing Score:")
                            .font(.caption.bold())
                            .foregroundColor(ThemeColors.tangerine)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    HStack {
                        Text(testResultsFormatHelper.writing(testResults: schoolDetailsModel.testResult))
                            .font(.caption2)
                            .foregroundColor(ThemeColors.mimosa)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    
                }
                .padding(.leading, 8.0)
                .padding(.trailing, 8.0)
                
                HStack {
                    Spacer()
                        .frame(height: 1.0)
                        .background(ThemeColors.gunmetal)
                }
                .padding(.horizontal, 8.0)
                
                VStack {
                    
                    HStack {
                        Text("Average Math Score:")
                            .font(.caption.bold())
                            .foregroundColor(ThemeColors.tangerine)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    HStack {
                        Text(testResultsFormatHelper.math(testResults: schoolDetailsModel.testResult))
                            .font(.caption2)
                            .foregroundColor(ThemeColors.mimosa)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    
                }
                .padding(.leading, 8.0)
                .padding(.trailing, 8.0)
                .padding(.bottom, 8.0)
                
                
            }
            .background(RoundedRectangle(cornerRadius: 8.0).foregroundColor(ThemeColors.darksteel))
            .background(RoundedRectangle(cornerRadius: 8.0).stroke().foregroundColor(ThemeColors.gunmetal))
        }
        .padding(.horizontal, 16.0)
        
    }
    
    func overviewParagraphSection() -> some View {
        VStack {
            VStack {
                HStack {
                    Text("Overview:")
                        .font(.caption.bold())
                        .foregroundColor(ThemeColors.cloud)
                        .fixedSize(horizontal: false, vertical: true)
                    Spacer()
                }
                .padding(.leading, 8.0)
                .padding(.trailing, 8.0)
                .padding(.top, 8.0)
                .padding(.bottom, 2.0)
                
                HStack {
                    Text(schoolFormatHelper.overviewParagraph(school: schoolDetailsModel.school))
                        .font(.caption2)
                        .foregroundColor(ThemeColors.cloud)
                        .fixedSize(horizontal: false, vertical: true)
                    Spacer()
                }
                .padding(.leading, 8.0)
                .padding(.trailing, 8.0)
                .padding(.bottom, 8.0)
            }
            .background(RoundedRectangle(cornerRadius: 8.0).foregroundColor(ThemeColors.darksteel))
            .background(RoundedRectangle(cornerRadius: 8.0).stroke().foregroundColor(ThemeColors.gunmetal))
        }
        .padding(.horizontal, 16.0)
        
    }
    
    func titleAddressSection() -> some View {
        VStack {
            HStack {
                Text(schoolFormatHelper.name(school: schoolDetailsModel.school))
                    .font(.title2)
                    .foregroundColor(ThemeColors.gunmetal)
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
            }
            .padding(.horizontal, 16.0)
            .padding(.bottom, 4.0)
            
            HStack {
                Text(schoolFormatHelper.addressLine1(school: schoolDetailsModel.school))
                    .font(.body)
                    .foregroundColor(ThemeColors.darksteel)
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
            }
            .padding(.horizontal, 16.0)
            
            HStack {
                Text(schoolFormatHelper.addressLine2(school: schoolDetailsModel.school))
                    .font(.body)
                    .foregroundColor(ThemeColors.darksteel)
                    .fixedSize(horizontal: false, vertical: true)
                Spacer()
            }
            .padding(.horizontal, 16.0)
            
            
            
        }
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView(schoolDetailsModel: SchoolDetailsModel.preview())
    }
}
