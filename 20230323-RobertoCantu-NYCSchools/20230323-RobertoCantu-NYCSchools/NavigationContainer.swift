//
//  NavigationContainer.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import SwiftUI

struct NavigationContainer: View {
    @ObservedObject var schoolListViewModel: SchoolListViewModel
    var body: some View {
        NavigationStack(path: $schoolListViewModel.navigation) {
            SchoolListView(schoolListViewModel: schoolListViewModel)
        }
    }
}

struct NavigationContainer_Previews: PreviewProvider {
    static var previews: some View {
        NavigationContainer(schoolListViewModel: SchoolListViewModel.preview())
    }
}
