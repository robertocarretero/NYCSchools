//
//  SchoolDetailsModel.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import Foundation
import NYCN

struct SchoolDetailsModel {
    let school: NYCNSchool
    let testResult: NYCNTestResults
}

extension SchoolDetailsModel: Hashable {
    static func ==(lhs: SchoolDetailsModel, rhs: SchoolDetailsModel) -> Bool {
        lhs.school.dbn == rhs.school.dbn
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(school.dbn)
    }
}

extension SchoolDetailsModel {
    public static func preview() -> SchoolDetailsModel {
        let school = NYCNSchool.preview()
        let testResult = NYCNTestResults.preview()
        return SchoolDetailsModel(school: school, testResult: testResult)
    }
}
