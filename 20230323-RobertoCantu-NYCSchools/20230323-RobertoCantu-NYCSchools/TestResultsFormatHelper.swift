//
//  TestResultsFormatHelper.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import Foundation
import NYCN

struct TestResultsFormatHelper {
    
    func takers(testResults: NYCNTestResults) -> String {
        return testResults.numOfSatTestTakers
    }
    
    func math(testResults: NYCNTestResults) -> String {
        return testResults.satMathAvgScore
    }
    
    func writing(testResults: NYCNTestResults) -> String {
        return testResults.satWritingAvgScore
    }
    
    func reading(testResults: NYCNTestResults) -> String {
        return testResults.satCriticalReadingAvgScore
    }
}
    
