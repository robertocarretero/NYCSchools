//
//  _0230323_RobertoCantu_NYCSchoolsApp.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/23/23.
//

import SwiftUI
import NYCN

@main
struct _0230323_RobertoCantu_NYCSchoolsApp: App {
    let schoolListViewModel = SchoolListViewModel(networkController: NYCNNetworkController(enviroment: .development))
    var body: some Scene {
        WindowGroup {
            NavigationContainer(schoolListViewModel: schoolListViewModel)
        }
    }
}
