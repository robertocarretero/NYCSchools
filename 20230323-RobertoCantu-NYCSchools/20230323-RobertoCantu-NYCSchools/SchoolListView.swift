//
//  SchoolListView.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import SwiftUI
import NYCN

struct SchoolListView: View {
    
    @ObservedObject var schoolListViewModel: SchoolListViewModel
    let schoolFormatHelper = SchoolFormatHelper()
    
    var body: some View {
        VStack {
            List(schoolListViewModel.schools) { school in
                Button {
                    schoolListViewModel.select(school: school)
                } label: {
                    schoolRow(school: school)
                }
                .listRowInsets(EdgeInsets())
                .listRowSeparator(.hidden)
            }
            .listStyle(PlainListStyle())
        }
        .alert("Sorry, no test scores are available, please try again", isPresented: $schoolListViewModel.testResultsError) {
            Button("Fine") { }
        }
        .alert("Sorry, no schools are available, please try again", isPresented: $schoolListViewModel.testResultsError) {
            Button("Fine") { }
        }
        .navigationDestination(for: SchoolDetailsModel.self) { schoolDetailsModel in
            SchoolDetailsView(schoolDetailsModel: schoolDetailsModel)
        }
    }
    
    func schoolRow(school: NYCNSchool) -> some View {
        VStack {
            VStack {
                
                HStack {
                    Image(systemName: "graduationcap.fill")
                        .resizable()
                        .frame(width: 24.0, height: 24.0)
                        .foregroundColor(ThemeColors.tangerine)
                        .padding(.leading, 12.0)
                        .padding(.trailing, 6.0)
                    Text(schoolFormatHelper.name(school: school))
                        .foregroundColor(ThemeColors.cloud)
                        .fixedSize(horizontal: false, vertical: true)
                    Spacer()
                    Image(systemName: "chevron.forward.square")
                        .resizable()
                        .frame(width: 28.0, height: 28.0)
                        .foregroundColor(ThemeColors.cloud)
                        .padding(.leading, 6.0)
                        .padding(.trailing, 12.0)
                }
                .padding(.all, 8.0)
            }
            .background(RoundedRectangle(cornerRadius: 8.0).foregroundColor(ThemeColors.darksteel))
            .background(RoundedRectangle(cornerRadius: 8.0).stroke().foregroundColor(ThemeColors.gunmetal))
            .padding(.horizontal, 16.0)
            .padding(.vertical, 4.0)
        }
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView(schoolListViewModel: SchoolListViewModel.preview())
    }
}
