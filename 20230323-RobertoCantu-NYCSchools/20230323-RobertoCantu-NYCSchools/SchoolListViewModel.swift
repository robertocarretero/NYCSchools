//
//  SchoolListViewModel.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import Foundation
import SwiftUI
import NYCN

class SchoolListViewModel: ObservableObject {
    
    @Published var schoolsError = false
    @Published var testResultsError = false
    @Published var navigation = NavigationPath()
    @Published var schools = [NYCNSchool]()
    
    let networkController: NYCNNetworkController
    init(networkController: NYCNNetworkController) {
        self.networkController = networkController
        fetchSchools()
    }
    
    func fetchSchools() {
        Task {
            if let _schools = try? await networkController.fetchSchools() {
                await MainActor.run {
                    self.schools = _schools
                }
            } else {
                await MainActor.run {
                    schoolsError = true
                }
            }
        }
    }
    
    func select(school: NYCNSchool) {
        Task {
            if let testResults = try? await networkController.fetchTestResults(dbn: school.dbn), testResults.count > 0 {
                let schoolDetailsModel = SchoolDetailsModel(school: school, testResult: testResults[0])
                await MainActor.run {
                    navigation.append(schoolDetailsModel)
                }
            } else {
                await MainActor.run {
                    testResultsError = true
                }
            }
        }
    }
}

extension SchoolListViewModel {
    public static func preview() -> SchoolListViewModel {
        let networkController = NYCNNetworkController(enviroment: .development)
        return SchoolListViewModel(networkController: networkController)
    }
}
