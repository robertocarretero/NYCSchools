//
//  SchoolFormatHelper.swift
//  20230323-RobertoCantu-NYCSchools
//
//  Created by Roberto Carretero on 3/24/23.
//

import Foundation
import NYCN

struct SchoolFormatHelper {
    
    func addressLine1(school: NYCNSchool) -> String {
        return school.primaryAddressLine1 ?? ""
    }

    func addressLine2(school: NYCNSchool) -> String {
        let city = school.city ?? ""
        let state = school.stateCode ?? ""
        let zip = school.zip ?? ""
        return "\(city), \(state) \(zip)"
    }
    
    func name(school: NYCNSchool) -> String {
        return school.schoolName ?? ""
    }
    
    func overviewParagraph(school: NYCNSchool) -> String {
        return school.overviewParagraph ?? ""
    }
    
    func website(school: NYCNSchool) -> String {
        return school.website ?? ""
    }
    
    func email(school: NYCNSchool) -> String {
        return school.schoolEmail ?? ""
    }
    
    func phone(school: NYCNSchool) -> String {
        return school.phoneNumber ?? ""
    }
    
    func fax(school: NYCNSchool) -> String {
        return school.faxNumber ?? ""
    }
}


