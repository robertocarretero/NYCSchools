import XCTest
@testable import NYCN

final class NYCNTests: XCTestCase {
    
    func testCertificate() {
        var certificateBank = NYCNCertificateBank()
        certificateBank.loadMainCertificate()
        XCTAssertGreaterThan(certificateBank.certificates.count, 0)
    }
    
    func testSchools() async throws {
        let networkController = NYCNNetworkController(enviroment: .development)
        let schools = try await networkController.fetchSchools()
        XCTAssertGreaterThan(schools.count, 0)
    }
    
    func testTestResults() async throws {
        let networkController = NYCNNetworkController(enviroment: .development)
        
        let schools = try await networkController.fetchSchools()
        
        guard schools.count > 5 else {
            XCTFail("Missing schools.")
            return
        }
        
        for index in 0..<5 {
            let school = schools[index]
            let testResults = try await networkController.fetchTestResults(dbn: school.dbn)
            if testResults.count > 0 {
                print("got \(testResults.count) for \(school.schoolName ?? "")")
                return
            }
        }
        
        XCTFail("Did not match any scores with schools.")
    }
}
