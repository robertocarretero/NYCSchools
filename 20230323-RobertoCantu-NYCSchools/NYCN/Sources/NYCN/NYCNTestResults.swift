//
//  NYCNTestResults.swift
//  
//
//  Created by Roberto Carretero on 3/23/23.
//

import Foundation

public struct NYCNTestResults: Decodable {
    public let dbn: String
    public let numOfSatTestTakers: String
    public let satCriticalReadingAvgScore: String
    public let satMathAvgScore: String
    public let satWritingAvgScore: String
}

extension NYCNTestResults: Identifiable {
    public var id: String { dbn }
}

extension NYCNTestResults {
    public static func preview() -> NYCNTestResults {
        NYCNTestResults(dbn: "14K477",
                        numOfSatTestTakers: "134",
                        satCriticalReadingAvgScore: "413",
                        satMathAvgScore: "396",
                        satWritingAvgScore: "395")
    }
}
