//
//  NYCNEnvironment.swift
//  
//
//  Created by Roberto Carretero on 3/23/23.
//

import Foundation

public enum NYCNEnvironment {
    case development
    case staging
    case production
}
