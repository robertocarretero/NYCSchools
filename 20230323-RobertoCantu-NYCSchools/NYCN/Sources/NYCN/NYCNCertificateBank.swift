//
//  NYCNCertificateBank.swift
//  
//
//  Created by Roberto Carretero on 3/23/23.
//

import Foundation

struct NYCNCertificateBank {
    
    private(set) var certificates = [Data]()
    
    init() {
        
    }
    
    mutating func loadMainCertificate() {
        
        guard let url = Bundle.module.url(forResource: "data.cityofnewyork.us", withExtension: "cer") else {
            print("Could not load certificate file: \"data.cityofnewyork.us.cer\"")
            return
        }
        
        do {
            let data = try Data(contentsOf: url)
            certificates.append(data)
            
        } catch let error {
            print("Error loading certificate file: \(error.localizedDescription)")
        }
    }
    
    func trust(certificates: [Data]) -> Bool {
        for serverData in certificates {
            for clientData in self.certificates {
                if serverData == clientData {
                    return true
                }
            }
        }
        return false
    }
    
    
}
