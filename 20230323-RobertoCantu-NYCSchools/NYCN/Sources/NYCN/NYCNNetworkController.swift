//
//  NYCNNetworkController.swift
//  
//
//  Created by Roberto Carretero on 3/23/23.
//

import Foundation
import Alamofire

public class NYCNNetworkController: SessionDelegate {
    
    let enviroment: NYCNEnvironment
    
    private lazy var certBank: NYCNCertificateBank = {
        var result = NYCNCertificateBank()
        result.loadMainCertificate()
        return result
    }()
    
    private lazy var session: Session = {
        let result = Session(configuration: .ephemeral, delegate: self)
        return result
    }()
    
    private lazy var decoder: JSONDecoder = {
        let result = JSONDecoder()
        result.keyDecodingStrategy = .convertFromSnakeCase
        return result
    }()
    
    public init(enviroment: NYCNEnvironment) {
        self.enviroment = enviroment
    }
    
    public func fetchSchools() async throws -> [NYCNSchool] {
        let url: URL?
        switch enviroment {
        case .development:
            url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        case .staging:
            url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        case .production:
            url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        }
        return try await fetch(url: url, model: [NYCNSchool].self)
    }
    
    public func fetchTestResults(dbn: String) async throws -> [NYCNTestResults] {
        let url: URL?
        switch enviroment {
        case .development:
            url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)")
        case .staging:
            url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)")
        case .production:
            url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)")
        }
        return try await fetch(url: url, model: [NYCNTestResults].self)
    }
    
    private func fetch<Model: Decodable>(url: URL?, model: Model.Type) async throws -> Model {
        return try await withCheckedThrowingContinuation { continuation in
            self.fetch(url: url, model: model.self) { result in
                switch result {
                case .success(let object):
                    continuation.resume(with: .success(object))
                case .failure(let error):
                    continuation.resume(throwing: error)
                }
            }
        }
    }
    
    private func fetch<Model: Decodable>(url: URL?,
                                         model: Model.Type,
                                         completion: @escaping (Result<Model, URLError>) -> Void) {
        guard let url = url else {
            completion(.failure(URLError(.badURL)))
            return
        }
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 16.0)
        session.download(request)
            .responseDecodable(of: Model.self, decoder: decoder) { a in
                switch a.result {
                case .success(let object):
                    completion(.success(object))
                case .failure(let error):
                    print("Network Error: \(error)")
                    completion(.failure(URLError(.badServerResponse)))
                }
            }
    }
}

extension NYCNNetworkController {
    
    // MARK: - SSL Pinning
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        guard let serverTrust = challenge.protectionSpace.serverTrust else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }
        
        guard let serverSecCertificates = SecTrustCopyCertificateChain(serverTrust) as? [SecCertificate] else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }
        
        let serverCertificates = serverSecCertificates.map {
            SecCertificateCopyData($0) as Data
        }
        
        if certBank.trust(certificates: serverCertificates) {
            completionHandler(.useCredential,
                              URLCredential(trust: serverTrust))
            
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
        
    }
}
