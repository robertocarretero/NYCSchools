//
//  NYCNSchool.swift
//  
//
//  Created by Roberto Carretero on 3/23/23.
//

import Foundation

public struct NYCNSchool: Decodable {
    public let dbn: String
    public let schoolName: String?
    public let overviewParagraph: String?
    public let website: String?
    public let schoolEmail: String?
    public let phoneNumber: String?
    public let faxNumber: String?
    
    public let primaryAddressLine1: String?
    public let city: String?
    public let zip: String?
    public let stateCode: String?
}

extension NYCNSchool: Identifiable {
    public var id: String { dbn }
}

extension NYCNSchool {
    public static func preview() -> NYCNSchool {
        NYCNSchool(dbn: "02M260",
                   schoolName: "Clinton School Writers & Artists, M.S. 260",
                   overviewParagraph: "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
                   website: "www.theclintonschool.net",
                   schoolEmail: "admissions@theclintonschool.net",
                   phoneNumber: "212-524-4360",
                   faxNumber: "212-524-4365",
                   primaryAddressLine1: "10 East 15th Street",
                   city: "Manhattan",
                   zip: "10003",
                   stateCode: "NY")
    }
}
